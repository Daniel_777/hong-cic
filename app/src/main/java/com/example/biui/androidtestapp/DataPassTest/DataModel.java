package com.example.biui.androidtestapp.DataPassTest;

/**
 * Created by jun on 2016-01-25.
 */
public class DataModel {
    private String data01;
    private String data02;

    DataModel(String data1, String data2){
        setData01(data1);
        setData02(data2);
    }

    public void setData01(String data01) {
        this.data01 = data01;
    }

    public String getData01() {
        return data01;
    }

    public void setData02(String data02) {
        this.data02 = data02;
    }

    public String getData02() {
        return data02;
    }

    @Override
    public String toString(){
        return "data01 = "+data01 + " // data02 = " +data02;
    }
}
