package com.example.biui.androidtestapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.biui.androidtestapp.R;

public class EditTextTest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setTitle("EditTextTest");
        setContentView(R.layout.activity_edit_text_test);
    }
}
