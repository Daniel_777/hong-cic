package com.example.biui.androidtestapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.biui.androidtestapp.DataPassTest.PassDataTest;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView spinnerTestBtn, logTestBtn, passDataTestBtn, editTextTestBtn, TextInputLayoutTestBtn, sssssssBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }
    private void init(){
        spinnerTestBtn = (TextView)findViewById(R.id.spinner_test);
        logTestBtn = (TextView)findViewById(R.id.log_test);
        passDataTestBtn = (TextView)findViewById(R.id.Intent_pass_data_test);
        editTextTestBtn = (TextView)findViewById(R.id.edit_text_test);
        TextInputLayoutTestBtn = (TextView)findViewById(R.id.text_input_layout_test);
        sssssssBtn = (TextView)findViewById(R.id.sssssss);
        spinnerTestBtn.setOnClickListener(this);
        logTestBtn.setOnClickListener(this);
        passDataTestBtn.setOnClickListener(this);
        editTextTestBtn.setOnClickListener(this);
        TextInputLayoutTestBtn.setOnClickListener(this);
        sssssssBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.spinner_test:
                startActivity(new Intent(MainActivity.this, SpinnerTest.class));
                break;
            case R.id.log_test:
                startActivity(new Intent(MainActivity.this, LogTest.class));
                break;
            case R.id.Intent_pass_data_test:
                startActivity(new Intent(MainActivity.this, PassDataTest.class));
                break;
            case R.id.edit_text_test:
                startActivity(new Intent(MainActivity.this, EditTextTest.class));
                break;
            case R.id.text_input_layout_test:
                startActivity(new Intent(MainActivity.this, TextInputLayoutTest.class));
                break;
            case R.id.sssssss:
                startActivity(new Intent(MainActivity.this, ParkEx.class));
                break;
        }
    }
}
