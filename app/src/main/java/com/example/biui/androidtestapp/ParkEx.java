package com.example.biui.androidtestapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ParkEx extends AppCompatActivity {
    TextView tv01;
    Button btn01;
    EditText edt10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_park_ex);

        tv01 = (TextView)findViewById(R.id.tv1);
        btn01 = (Button)findViewById(R.id.btn1);
        edt10 = (EditText)findViewById(R.id.edt1);

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv01.setText(edt10.getText().toString());
            }
        });
    }
}
