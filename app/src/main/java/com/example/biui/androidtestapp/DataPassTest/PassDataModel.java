package com.example.biui.androidtestapp.DataPassTest;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jun on 2016-01-25.
 */
public class PassDataModel implements Serializable{
    private String data1;
    private String data2;
    private ArrayList<DataModel> dataList;
    PassDataModel(){

    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public ArrayList<DataModel> getDataList() {
        return dataList;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public String getData1() {
        return data1;
    }

    public void setDataList(ArrayList<DataModel> dataList) {
        this.dataList = dataList;
    }

    public String getData2() {
        return data2;
    }

    @Override
    public String toString(){
        return "data1 = "+data1 + " // data2 = " +data2 + " // " + dataList.get(0).toString();
    }
}
