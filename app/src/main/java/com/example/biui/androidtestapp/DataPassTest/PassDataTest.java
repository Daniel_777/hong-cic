package com.example.biui.androidtestapp.DataPassTest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.biui.androidtestapp.R;

import java.util.ArrayList;

public class PassDataTest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_data_test);

        PassDataModel dataModel = new PassDataModel();
        dataModel.setData1("Data1Data1Data1");
        dataModel.setData2("Data2Data2Data2");
        ArrayList<DataModel> list = new ArrayList<DataModel>();
        list.add(new DataModel("data11data11data11", "data22data22data22"));
        dataModel.setDataList(list);

        Intent intent = new Intent(PassDataTest.this, ReceiveData.class);
        intent.putExtra("testData", dataModel);
        startActivity(intent);
    }
}
