package com.example.biui.androidtestapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class LogTest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("LogTest");
        setContentView(R.layout.activity_log_test);

        Log.e("test", "error ");
        Log.w("test", "warning ");
        Log.i("test", "information ");
        Log.d("test", "debug ");
        Log.v("test", "verbose ");
    }
}
