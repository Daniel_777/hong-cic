package com.example.biui.androidtestapp.DataPassTest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.biui.androidtestapp.R;

public class ReceiveData extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_data);

        Intent intent = getIntent();
        PassDataModel dataModel = (PassDataModel)intent.getSerializableExtra("testData");
        Toast.makeText(ReceiveData.this, " "+ dataModel.toString(), Toast.LENGTH_SHORT).show();
    }
}
